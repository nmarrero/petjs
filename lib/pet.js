// Generated by CoffeeScript 1.7.1
(function() {
  var PetApplication,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  PetApplication = (function(_super) {
    var regions;

    __extends(PetApplication, _super);

    function PetApplication() {
      this.onStart = __bind(this.onStart, this);
      return PetApplication.__super__.constructor.apply(this, arguments);
    }

    regions = {
      header: "#header",
      content: "#content",
      footer: "#footer"
    };

    PetApplication.prototype.onStart = function() {
      return header.show();
    };

    return PetApplication;

  })(Backbone.Marionette.Application);

  window.pet = new PetApplication();

}).call(this);

//# sourceMappingURL=pet.map
