class PetApplication extends Backbone.Marionette.Application

    regions = 
        header: "#header"
        content: "#content"
        footer: "#footer"
        
        
    onStart: =>
        header.show()
    


window.pet = new PetApplication()